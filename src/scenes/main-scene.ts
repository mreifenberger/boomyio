/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 - 2019 digitsensitive
 * @license      {@link https://github.com/digitsensitive/phaser3-typescript/blob/master/LICENSE.md | MIT License}
 */

export class MainScene extends Phaser.Scene {


  player: any;


  constructor() {
    super({
      key: "MainScene"
    });
  }

  preload(): void {
    this.load.image("myImage", "../src/assets/phaser.png");
      this.load.image("sky", "../src/assets/sky.png");
      this.load.image("ground", "../src/assets/platform.png");
      this.load.image("star", "../src/assets/star.png");
      this.load.image("bomb", "../src/assets/bomb.png");
      this.load.spritesheet("dude",
          "../src/assets/dude.png",
          { frameWidth: 32, frameHeight: 48 }
      );
  }

  update(): void {
      if(this.input.keyboard.addKey('D').isDown || this.input.keyboard.addKey('A').isDown )
      {

      this.input.keyboard.on("keydown-A", () => {
          this.player.setVelocityX(-300);
          this.player.anims.play('left', true);
      });

      this.input.keyboard.on("keydown-D", () => {
          this.player.setVelocityX(300);
          this.player.anims.play('right', true);
      });

      this.input.keyboard.on("keydown-S", () => {

          this.player.setVelocityX(0);
          this.player.anims.play('turn');

      });

      this.input.keyboard.on("keydown-W", () => {
          if(this.player.body.touching.down) {
              this.player.setVelocityY(-330);
          }

      });

      } else {
        this.player.setVelocityX(0);
        this.player.anims.play('turn');
      }
  }

  create(): void {
      console.log("physics");
      console.log(this.physics);

      this.add.image(400,300, "sky");

      let platforms = this.physics.add.staticGroup();

      platforms.create(400, 568, 'ground').setScale(2).refreshBody();
      platforms.create(600, 400, 'ground');
      platforms.create(50, 250, 'ground');
      platforms.create(750, 220, 'ground');

      this.player = this.physics.add.sprite(100, 450, 'dude');

      this.player.setCollideWorldBounds(true);

      this.anims.create({
          key: 'left',
          frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
          frameRate: 10,
          repeat: -1
      });

      this.anims.create({
          key: 'turn',
          frames: [ { key: 'dude', frame: 4 } ],
          frameRate: 20
      });

      this.anims.create({
          key: 'right',
          frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
          frameRate: 10,
          repeat: -1
      });

      this.physics.add.collider(this.player, platforms);
  }
}
