const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const uuid = require("uuid");

app.use(express.static("public"));

app.get("/", (req: Request, res: Response) => {
    res.sendfile('./index.html');
});